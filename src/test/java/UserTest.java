import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class UserTest {
    @Test
    public void testUserNameTooShort() {

        try {
            User user = new User();
            user.setName("joakdjshgkjahdslkgajhdsgkjahsdgkjahds");

//            fail();
        } catch (IllegalArgumentException ex) {
            assertEquals("Username is too long", ex.getMessage());
        }
    }
}

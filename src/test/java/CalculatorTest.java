import org.junit.jupiter.api.*;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {
    private Calculator calculator = new Calculator();

//    @BeforeAll
//    public static void beforeAll(){
//        System.out.println("Before all");
//    }
//
//    @AfterAll
//    public static void afterAll() {
//        System.out.println("After all");
//    }
//
//    @BeforeEach
//    public void setUp() {
//        System.out.println("Before each");
//    }
//
//    @AfterEach
//    public void tearDown() {
//        System.out.println("After each");
//    }

    @DisplayName("Positive Test calculator")
    @Test
    public void testAddSuccess() {
        var result = calculator.add(10,10);
        assertEquals(20, result);
    }

    @Test
    public void testPersegiSuccess(){
        var hasil = calculator.hitungPersegi(10);
        assertEquals(100,hasil);
    }

    @Test
    public void testPersegiFailed() {
        assertThrows(IllegalArgumentException.class, () -> {
            calculator.hitungPersegi(0);
        });
    }

    @Test
    public void testDivideSuccess() {
        var result = calculator.divide(100, 10);
        assertEquals(10, result);
    }

    @Test
    public void testDivideFailed() {
        assertThrows(IllegalArgumentException.class, () -> {
            calculator.divide(10, 0);
        });
    }
}
